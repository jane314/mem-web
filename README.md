# mem-web

This is not in the most user friendly state at the moment.

Use the `spec.json` here with [Corolla](https://gitlab.com/jane314/corolla) and
[Caddy](https://caddyserver.com/) for the backend.

For the frontend, change directory to `./frontend` and run
`npm i; npm run build`.
