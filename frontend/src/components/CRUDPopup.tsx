import { useEffect, useState } from "react";
import "../App.css";
import "./CRUDPopup.css";
import { WorkingSpinner } from "./WorkingSpinner";
import { writeQuery } from "../API";
import { date2str } from "../Lib";

function CRUDPopup(props: {
  visible: boolean;
  setVisible: (value: React.SetStateAction<boolean>) => void;
  hydrate: () => void;
  closeHook?: () => void;
  receivedData?: {
    id: string;
    date: string;
    note: string;
    tags: string;
    hasDueDate: string;
  };
  receiveKeyBinding?: KeyboardEvent;
}) {
  /*
   * state
   */
  const [date, setDate] = useState(
    date2str(new Date()),
  );
  const [note, setNote] = useState("");
  const [tags, setTags] = useState("");
  const [hasDueDate, setHasDueDate] = useState(true);
  useEffect(() => {
    if (props.receivedData !== undefined) {
      setDate(props.receivedData.date);
      setNote(props.receivedData.note);
      setTags(props.receivedData.tags);
      setHasDueDate(props.receivedData.hasDueDate ? true : false);
    }
  }, [props.receivedData]);
  useEffect(() => {
    if (props.visible) {
      if (
        (props.receiveKeyBinding?.key === "s") &&
        (props.receiveKeyBinding?.altKey)
      ) {
        saveAndClose();
      } else if (props.receiveKeyBinding?.key === "Escape") {
        if (props.closeHook !== undefined) {
          props.closeHook();
        }
        props.setVisible(false);
      }
    }
  }, [props.receiveKeyBinding]);
  const [working, setWorking] = useState(false);
  /*
   * Aux functions
   */
  function saveAndClose() {
    setWorking(true);
    let query: Promise<boolean>;
    if (props.receivedData === undefined) {
      query = writeQuery({
        query_name: "add",
        args: {
          date: hasDueDate ? date : "",
          note,
          tags,
          hasduedate: hasDueDate ? "1" : "0",
        },
      });
    } else {
      query = writeQuery({
        query_name: "update",
        args: {
          id: props.receivedData.id,
          date: hasDueDate ? date : "",
          note,
          tags,
          hasduedate: hasDueDate ? "1" : "0",
        },
      });
    }
    query.then(() => {
      props.setVisible(false);
      setWorking(false);
      props.hydrate();
      setHasDueDate(true);
      setDate(date2str(new Date()));
      setNote("");
      setTags("");
    });
  }
  /*
   * CRUD popup
   */
  if (props.visible) {
    return (
      <div className="crudpopup">
        {props.receivedData !== undefined
          ? <span className="title">{"Editing " + props.receivedData.id}</span>
          : <span className="title">New Task</span>}
        <div className="crud-row">
          <input
            type="tags"
            placeholder="tags"
            value={tags}
            onChange={(e) => setTags(e.target.value)}
          />
        </div>
        <div className="crud-row">
          <input
            type="checkbox"
            checked={hasDueDate}
            onChange={(e) => setHasDueDate(e.target.checked)}
          />
          <span>Include due date?</span>
        </div>
        <div className="crud-row">
          <input
            type="date"
            value={date}
            disabled={!hasDueDate}
            onChange={(e) => setDate(e.target.value)}
          />
        </div>
        <div className="crud-row">
          <textarea
            autoFocus={true}
            className="crudpopup-textarea"
            rows={12}
            placeholder="Your note goes here"
            value={note}
            onChange={(e) => setNote(e.target.value)}
          >
          </textarea>
        </div>
        <div className="crud-row">
          <input
            type="button"
            value="Save"
            onClick={saveAndClose}
          />
          <input
            type="button"
            value="Cancel"
            onClick={() => {
              if (props.closeHook !== undefined) {
                props.closeHook();
              }
              props.setVisible(false);
            }}
          />
          <WorkingSpinner visible={working} />
        </div>
      </div>
    );
  }
  return <></>;
}

export { CRUDPopup };
