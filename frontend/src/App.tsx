import { useEffect, useRef, useState } from "react";
import "./App.css";
import { CRUDPopup } from "./components/CRUDPopup";
import { readQuery, writeQuery } from "./API";
import { date2str } from "./Lib";

function App() {
  /*
   * state
   */
  const [newItemInput, setNewItemInput] = useState(false);
  const [oldItemEdit, setOldItemEdit] = useState(false);
  const [oldItemEditData, setOldItemEditData] = useState<{
    id: string;
    date: string;
    note: string;
    tags: string;
    hasDueDate: string;
  }>({ id: "", date: "", note: "", tags: "", hasDueDate: "" });
  const [selectedRow, setSelectedRow] = useState(0);
  const [rows, setRows] = useState<string[][]>([[]]);
  const [hydrateAux, setHydrateAux] = useState(true);
  const [keyBinding, setKeyBinding] = useState<KeyboardEvent>();
  function hydrate() {
    setHydrateAux(!hydrateAux);
  }
  useEffect(() => {
    readQuery({ query_name: "get_not_done", args: {} }).then(
      (res: string[][]) => {
        setRows(res);
      },
    );
  }, [hydrateAux]);
  /*
   * keybindings
   */
  const rowRef = useRef({} as { [id: number]: HTMLElement });
  useEffect(() => {
    window.addEventListener("keydown", (e) => {
      if (e !== undefined) {
        setKeyBinding(e);
      }
    });
  }, []);
  useEffect(() => {
    if (newItemInput || oldItemEdit) {
      return;
    }
    if (keyBinding?.key === "n") {
      setNewItemInput(true);
    } else if (keyBinding?.key === "j") {
      const newRow = (selectedRow + 1) % rows.length;
      setSelectedRow(newRow);
      rowRef.current[newRow].scrollIntoView({
        "behavior": "smooth",
        "block": "center",
      });
    } else if (keyBinding?.key === "k") {
      const newRow = (rows.length + selectedRow - 1) % rows.length;
      setSelectedRow(newRow);
      rowRef.current[newRow].scrollIntoView({
        "behavior": "smooth",
        "block": "center",
      });
    } else if (keyBinding?.key === "e") {
      const entry = rows[selectedRow];
      setOldItemEditData(
        {
          id: entry[0],
          date: entry[1] === "" ? date2str(new Date()) : entry[1],
          note: entry[3],
          tags: entry[2],
          hasDueDate: entry[5],
        },
      );
      setOldItemEdit(true);
    } else if (keyBinding?.key === "!") {
      writeQuery({
        query_name: "mark_done",
        args: { id: rows[selectedRow][0] },
      }).then(() => {
        hydrate();
      });
    }
  }, [keyBinding]);
  /*
   * App div
   */
  return (
    <>
      <div
        id="App"
        style={{ opacity: newItemInput || oldItemEdit ? "0.3" : "1" }}
      >
        <div className="titlebar">
          <span className="title">mem</span>
          <div>
            <input
              type="button"
              id="add-button"
              className="control"
              value="➕"
              onClick={() => setNewItemInput(true)}
            />
          </div>
        </div>
        <table id="grid">
          <tbody>
            <tr className="titlerow">
              <td className="data">ID</td>
              <td className="data">Date</td>
              <td className="data">Note</td>
              <td className="data">Tags</td>
              <td></td>
            </tr>
            {rows.map((entry, i) => (
              <tr
                key={entry[0]}
                ref={(e) => {
                  if (e !== null) {
                    rowRef.current[i] = e;
                  }
                }}
                style={(selectedRow === i)
                  ? { backgroundColor: "linen" }
                  : undefined}
              >
                <td className="data">{entry[0] || ""}</td>
                <td className="data">
                  {entry[5] ? entry[1] : ""}
                </td>
                <td className="data">
                  <pre className="note">{entry[3] || ""}</pre>
                </td>
                <td className="data mono">{entry[2]}</td>
                <td>
                  <input
                    type="button"
                    className="control"
                    value="📝"
                    onClick={() => {
                      setOldItemEditData(
                        {
                          id: entry[0],
                          date: entry[1] === ""
                            ? date2str(new Date())
                            : entry[1],
                          note: entry[3],
                          tags: entry[2],
                          hasDueDate: entry[5],
                        },
                      );
                      setOldItemEdit(true);
                    }}
                  />
                  <input
                    type="button"
                    className="control"
                    value="✔️"
                    onClick={() => {
                      writeQuery({
                        query_name: "mark_done",
                        args: { id: entry[0] },
                      }).then(() => {
                        hydrate();
                      });
                    }}
                  />
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
      {/* new item */}
      <CRUDPopup
        visible={newItemInput}
        setVisible={setNewItemInput}
        hydrate={hydrate}
        receiveKeyBinding={keyBinding}
      />
      {/* editing existing item */}
      <CRUDPopup
        visible={oldItemEdit}
        setVisible={setOldItemEdit}
        hydrate={hydrate}
        receivedData={oldItemEditData}
        receiveKeyBinding={keyBinding}
      />
    </>
  );
}

export default App;
