export interface ReqBody {
  query_name: string;
  args: {
    [key: string]: string | number | boolean;
  };
}

export async function readQuery(
  body: ReqBody,
): Promise<string[][]> {
  return await fetch(
    "/api/query?body=" + encodeURIComponent(JSON.stringify(body)),
  )
    .then((res) => res.json());
}

export async function writeQuery(body: ReqBody): Promise<boolean> {
  return await fetch(
    "/api/query",
    { method: "POST", body: JSON.stringify(body) },
  )
    .then((res) => res.json());
}
